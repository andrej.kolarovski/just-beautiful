export interface FilterParameter {
  [filter: string]: any;
}
