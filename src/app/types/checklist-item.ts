import {Ref} from './ref';

export class ChecklistItem extends Ref {
  description: string;
  completed: boolean;
}
