export class Ref {
  id: number;
  href: string;
  name: string;
}
