import {Checklist} from './checklist';

export class Storage {
  items: Array<Checklist>;
  count: number;
  selectedChecklistId: number;
}
