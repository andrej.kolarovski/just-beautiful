import {ChecklistItem} from './checklist-item';
import {Ref} from './ref';
import {Tag} from './tag';

export class Checklist extends Ref {
  description: string;
  items: Array<ChecklistItem>;
  tags: Array<Tag>;
}
