import { Routes } from '@angular/router';

import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { ChecklistEditComponent } from './components/checklist-edit/checklist-edit.component';

export const appRoutes: Routes = [
  { path: 'landing-page', component: LandingPageComponent },
  { path: 'edit', component: ChecklistEditComponent},
  { path: '', redirectTo: '/landing-page', pathMatch: 'full'}
];
