import { TestBed, inject } from '@angular/core/testing';

import { AppStorageService } from './app-storage.service';
import { LocalStorageService } from '../local-storage/local-storage.service';

describe('AppStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppStorageService, LocalStorageService]
    });
  });

  it('should be created', inject([AppStorageService], (service: AppStorageService) => {
    expect(service).toBeTruthy();
  }));
});
