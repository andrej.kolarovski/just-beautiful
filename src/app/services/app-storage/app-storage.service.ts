import { Injectable, Inject } from '@angular/core';

import { Storage } from '../../types/storage';
import { Checklist } from '../../types/checklist';
import { FilterParameter } from '../../types/filter';
import { LocalStorageService } from '../local-storage/local-storage.service';

import { IAppStorageService } from '../../domain/interfaces/app-storage-service';
import { IStorageFactory } from '../../domain/interfaces/storage-factory';

@Injectable()
export class AppStorageService implements IAppStorageService {
  private storage: Storage;

  constructor(
    private localStorageService: LocalStorageService,
    @Inject('IStorageFactory') private iStorageFactory: IStorageFactory,
  ) {
    this.initStorage();
  }

  initStorage = () => {
    this.storage = this.getLocalStorage() || this.newStorage();
  }

  newStorage = (): Storage => {
    return this.iStorageFactory.doBuild();
  }

  getStorage = (): Storage => {
    return this.storage;
  }

  getChecklists = (): Array<Checklist> => {
    if (!this.storage) { return; }
    return this.storage.items;
  }

  searchChecklists = (searchTerm: string): Array<Checklist> => {
    const checklists = this.getChecklists();
    if (!checklists) { return; }
    return checklists.filter(
      checklist => checklist.name.indexOf(searchTerm) > -1 || checklist.description.indexOf(searchTerm) > -1
    );
  }

  getChecklistCount = (): number => {
    if (!this.storage) { return; }
    return this.storage.count;
  }

  getSelectedChecklistId = (): number => {
    if (!this.storage) { return -1; }
    return this.storage.selectedChecklistId;
  }

  get = (id: number): Checklist => {
    if (!this.storage) { return; }
    const checklists = this.getChecklists();
    const currentIds = this.getIds();
    const index = currentIds.indexOf(id);
    return checklists[index];
  }

  select = (id: number) => {
    this.storage.selectedChecklistId = id;
  }

  update = (checklist: Checklist) => {
    const id = checklist.id;
    const checklists = this.getChecklists();
    const currentIds = this.getIds();
    const index = currentIds.indexOf(id);
    this.storage.items[index] = checklist;
  }

  add = (checklist: Checklist) => {
    this.storage.items.push(checklist);
    this.storage.count++;
  }

  delete = (checklist: Checklist) => {
    const id = checklist.id;
    const checklists = this.getChecklists();
    const currentIds = this.getIds();
    const index = currentIds.indexOf(id);
    checklists.splice(index, 1);
    this.storage.count--;
  }

  // returns next available index in items array
  nextId = (): number => {
    if (!this.storage) { return -1; }
    return this.iStorageFactory.nextId(this.storage);
  }

  getIds = (sort?: boolean): Array<number> => {
    return this.iStorageFactory.getIds(this.storage, sort);
  }

  getLocalStorage = (): Storage => {
    return this.localStorageService.getStorage();
  }

  updateLocalStorage = () => {
    this.localStorageService.setStorage(this.storage);
  }
}
