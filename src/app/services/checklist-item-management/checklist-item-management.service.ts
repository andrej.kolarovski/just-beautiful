import { Injectable, Inject } from '@angular/core';
import { Checklist } from '../../types/checklist';
import { ChecklistItem } from '../../types/checklist-item';

import { IChecklistItemManagementService } from '../../domain/interfaces/checklist-item-management-service';
import { IChecklistItemFactory } from '../../domain/interfaces/checklist-item-factory';

@Injectable()
export class ChecklistItemManagementService implements IChecklistItemManagementService {

  constructor(
    @Inject('IChecklistItemFactory') private iChecklistItemFactory: IChecklistItemFactory,
  ) { }

  newItem = (checklist: Checklist): ChecklistItem => {
    const id = this.nextId(checklist);
    return this.iChecklistItemFactory.doBuild(id);
  }

  addItem = (checklist: Checklist, item: ChecklistItem): Checklist => {
    checklist.items.push(item);
    return checklist;
  }

  removeItem = (checklist: Checklist, item: ChecklistItem): Checklist => {
    return;
  }

  nextId = (checklist: Checklist): number => {
    return this.iChecklistItemFactory.nextId(checklist);
  }
}
