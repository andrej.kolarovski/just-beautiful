import { TestBed, inject } from '@angular/core/testing';

import { ChecklistItemManagementService } from './checklist-item-management.service';

describe('ChecklistItemManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChecklistItemManagementService]
    });
  });

  it('should be created', inject([ChecklistItemManagementService], (service: ChecklistItemManagementService) => {
    expect(service).toBeTruthy();
  }));
});
