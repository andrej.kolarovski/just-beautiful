import { Injectable, Inject } from '@angular/core';

import { AppStorageService } from '../app-storage/app-storage.service';
import { Checklist } from '../../types/checklist';
import { Storage } from '../../types/storage';

import { IChecklistManagementService } from '../../domain/interfaces/checklist-management-service';
import { IChecklistFactory } from '../../domain/interfaces/checklist-factory';

@Injectable()
export class ChecklistManagementService implements IChecklistManagementService {

  constructor(
    private appStorageService: AppStorageService,
    @Inject('IChecklistFactory') private iChecklistFactory: IChecklistFactory,
  ) {}

  getChecklists = (): Array<Checklist> => {
    return this.appStorageService.getChecklists();
  }

  searchChecklists = (searchTerm: string): Array<Checklist> => {
    return this.appStorageService.searchChecklists(searchTerm);
  }

  getSelectedChecklist = (): Checklist => {
    const id = this.appStorageService.getSelectedChecklistId();
    if (id === -1) { return; }
    return this.appStorageService.get(id);
  }

  getChecklist = (id: number): Checklist => {
    return this.appStorageService.get(id);
  }

  selectChecklist = (id: number) => {
    this.appStorageService.select(id);
  }

  newChecklist = (): Checklist => {
    const nextId = this.appStorageService.nextId();
    const checklist = this.iChecklistFactory.doBuild(nextId);
    this.appStorageService.add(checklist);
    return checklist;
  }

  updateChecklist = (checklist: Checklist) => {
    this.appStorageService.update(checklist);
  }

  deleteChecklist = (checklist: Checklist) => {
    this.appStorageService.delete(checklist);
  }
}
