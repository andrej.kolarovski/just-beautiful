import { TestBed, inject } from '@angular/core/testing';

import { ChecklistManagementService } from './checklist-management.service';
import { AppStorageService } from '../app-storage/app-storage.service';
import { LocalStorageService } from '../local-storage/local-storage.service';

describe('ChecklistManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChecklistManagementService, AppStorageService, LocalStorageService]
    });
  });

  it('should be created', inject([ChecklistManagementService], (service: ChecklistManagementService) => {
    expect(service).toBeTruthy();
  }));
});
