import { Injectable } from '@angular/core';

import { Storage } from '../../types/storage';
import { ILocalStorageService } from '../../domain/interfaces/local-storage-service';

@Injectable()
export class LocalStorageService implements ILocalStorageService {

  constructor() { }

  clearStorage = () => {
    localStorage.removeItem('jb_stored_checklists');
  }

  storageInitialized = (): boolean => {
    return !!localStorage.getItem('jb_stored_checklists');
  }

  setStorage = (storage: Storage) => {
    localStorage.setItem('jb_stored_checklists', JSON.stringify(storage));
  }

  getStorage = (): Storage => {
    if (!this.storageInitialized()) { return; }
    return JSON.parse(localStorage.getItem('jb_stored_checklists'));
  }
}
