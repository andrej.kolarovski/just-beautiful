import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { appRoutes } from './app.routes';

// factories
import { ChecklistItemFactory } from './domain/factories/checklist-item.factory';
import { ChecklistFactory } from './domain/factories/checklist.factory';
import { StorageFactory } from './domain/factories/storage.factory';

import { AppComponent } from './app.component';
import { LocalStorageService } from './services/local-storage/local-storage.service';
import { ChecklistManagementService } from './services/checklist-management/checklist-management.service';
import { AppStorageService } from './services/app-storage/app-storage.service';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { HeaderComponent } from './components/header/header.component';
import { SimpleSearchComponent } from './components/simple-search/simple-search.component';
import { MyChecklistsComponent } from './components/my-checklists/my-checklists.component';
import { ChecklistPreviewComponent } from './components/checklist-preview/checklist-preview.component';
import { ChecklistItemPreviewComponent } from './components/checklist-item-preview/checklist-item-preview.component';
import { ChecklistEditComponent } from './components/checklist-edit/checklist-edit.component';
import { ChecklistItemEditComponent } from './components/checklist-item-edit/checklist-item-edit.component';
import { TagComponent } from './components/tag/tag.component';
import { ChecklistItemManagementService } from './services/checklist-item-management/checklist-item-management.service';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    HeaderComponent,
    SimpleSearchComponent,
    MyChecklistsComponent,
    ChecklistPreviewComponent,
    ChecklistItemPreviewComponent,
    ChecklistEditComponent,
    ChecklistItemEditComponent,
    TagComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule
  ],
  providers: [
    LocalStorageService,
    ChecklistManagementService,
    ChecklistItemManagementService,
    AppStorageService,
    {provide: 'IChecklistItemFactory', useClass: ChecklistItemFactory},
    {provide: 'IChecklistFactory', useClass: ChecklistFactory},
    {provide: 'IStorageFactory', useClass: StorageFactory},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
