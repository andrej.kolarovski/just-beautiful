import { Component } from '@angular/core';
import { HostListener } from '@angular/core';

import { AppStorageService } from './services/app-storage/app-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Just Beautiful';

  constructor(
    private appStorageService: AppStorageService
  ) {}

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler = (event: any) => {
    this.appStorageService.updateLocalStorage();
  }
}
