import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecklistItemPreviewComponent } from './checklist-item-preview.component';

describe('ChecklistItemPreviewComponent', () => {
  let component: ChecklistItemPreviewComponent;
  let fixture: ComponentFixture<ChecklistItemPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecklistItemPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecklistItemPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
