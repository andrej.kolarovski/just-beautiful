import { Component, OnInit, Input } from '@angular/core';

import { ChecklistItem } from '../../types/checklist-item';

@Component({
  selector: 'app-checklist-item-preview',
  templateUrl: './checklist-item-preview.component.html',
  styleUrls: ['./checklist-item-preview.component.css']
})
export class ChecklistItemPreviewComponent implements OnInit {
  @Input() item: ChecklistItem;

  constructor() { }

  ngOnInit() {
  }

}
