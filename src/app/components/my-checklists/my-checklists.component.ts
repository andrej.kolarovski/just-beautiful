import { Component, OnInit } from '@angular/core';

import { Checklist } from '../../types/checklist';
import {ChecklistItem} from '../../types/checklist-item';

import { ChecklistManagementService } from '../../services/checklist-management/checklist-management.service';

@Component({
  selector: 'app-my-checklists',
  templateUrl: './my-checklists.component.html',
  styleUrls: ['./my-checklists.component.css']
})
export class MyChecklistsComponent implements OnInit {

  constructor(
    private checklistManagementService: ChecklistManagementService,
  ) { }

  ngOnInit() {
  }

  getChecklistsArray = (): Array<Checklist> => {
    return this.checklistManagementService.getChecklists();
  }
}
