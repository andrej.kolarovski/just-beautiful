import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyChecklistsComponent } from './my-checklists.component';

describe('MyChecklistsComponent', () => {
  let component: MyChecklistsComponent;
  let fixture: ComponentFixture<MyChecklistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyChecklistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyChecklistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
