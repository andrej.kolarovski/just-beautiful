import { Component, OnInit } from '@angular/core';

import { Checklist } from '../../types/checklist';
import { ChecklistItem } from '../../types/checklist-item';
import { Tag } from '../../types/tag';

import { ChecklistManagementService } from '../../services/checklist-management/checklist-management.service';
import { ChecklistItemManagementService } from '../../services/checklist-item-management/checklist-item-management.service';

@Component({
  selector: 'app-checklist-edit',
  templateUrl: './checklist-edit.component.html',
  styleUrls: ['./checklist-edit.component.css']
})
export class ChecklistEditComponent implements OnInit {
  private checklist: Checklist;

  constructor(
    private checklistManagementService: ChecklistManagementService,
    private checklistItemManagementService: ChecklistItemManagementService,
  ) { }

  ngOnInit() {
    this.checklist = this.checklistManagementService.getSelectedChecklist();
  }

  getTagsArray = (): Array<Tag> => {
    return this.checklist.tags;
  }

  getItemsArray = (): Array<ChecklistItem> => {
    return this.checklist.items;
  }

  onItemEnter = (index: number) => {
    const items = this.getItemsArray();
    if (index === items.length - 1) {
      this.addNewItem();
    }
    this.focusOnNextItem();
  }

  addNewItem = () => {
    const newItem = this.checklistItemManagementService.newItem(this.checklist);
    this.checklist = this.checklistItemManagementService.addItem(this.checklist, newItem);
    this.focusOnNextItem();
  }

  onNameKeypress = (event: any) => {
    switch (event.keyCode) {
      case 13: return this.onNameEnterPress(event);
      default: return;
    }
  }

  onNameEnterPress = (event: any) => {
    if (
      !this.checklist.name ||
      !this.checklist.name.length ||
      this.getItemsArray().length
    ) { return this.focusOnNextItem(); }
    this.addNewItem();
    this.focusOnNextItem();
  }

  // todo: move to directive, make better
  focusOnNextItem = () => {
    setTimeout(() => {
      const inputs = document.getElementsByTagName('input');
      const lastInput = inputs[inputs.length - 1];
      lastInput.focus();
    });
  }
}
