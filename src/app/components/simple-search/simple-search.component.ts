import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Checklist } from '../../types/checklist';

import { ChecklistManagementService } from '../../services/checklist-management/checklist-management.service';

@Component({
  selector: 'app-simple-search',
  templateUrl: './simple-search.component.html',
  styleUrls: ['./simple-search.component.css']
})
export class SimpleSearchComponent implements OnInit {

  constructor(
    private checklistManagementService: ChecklistManagementService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  newChecklist = () => {
    const checklist = this.checklistManagementService.newChecklist();
    this.checklistManagementService.selectChecklist(checklist.id);
    this.goToEdit();
  }

  goToEdit = () => {
    this.router.navigate(['/edit']);
  }
}
