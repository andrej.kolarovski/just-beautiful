import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecklistItemEditComponent } from './checklist-item-edit.component';

describe('ChecklistItemEditComponent', () => {
  let component: ChecklistItemEditComponent;
  let fixture: ComponentFixture<ChecklistItemEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecklistItemEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecklistItemEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
