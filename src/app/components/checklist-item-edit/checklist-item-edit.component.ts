import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { ChecklistItem } from '../../types/checklist-item';

@Component({
  selector: 'app-checklist-item-edit',
  templateUrl: './checklist-item-edit.component.html',
  styleUrls: ['./checklist-item-edit.component.css']
})
export class ChecklistItemEditComponent implements OnInit {
  @Input() item: ChecklistItem;
  @Output() enterPress: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onNameKeypress = (event: any) => {
    switch (event.keyCode) {
      case 13: return this.onNameEnterPress();
      default: return;
    }
  }

  onNameEnterPress = () => {
    if (!this.item.name || !this.item.name.length) { return; }
    this.enterPress.emit();
  }

  toggleCompleted = () => {
    this.item.completed = !this.item.completed;
  }
}
