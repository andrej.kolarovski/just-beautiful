import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Checklist } from '../../types/checklist';
import { ChecklistItem } from '../../types/checklist-item';

import { ChecklistManagementService } from '../../services/checklist-management/checklist-management.service';

@Component({
  selector: 'app-checklist-preview',
  templateUrl: './checklist-preview.component.html',
  styleUrls: ['./checklist-preview.component.css']
})
export class ChecklistPreviewComponent implements OnInit {
  @Input() checklist: Checklist;

  constructor(
    private checklistManagementService: ChecklistManagementService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  getItemsArray = (): Array<ChecklistItem> => {
    return this.checklist.items;
  }

  getHiddenItemsCount = (): number => {
    return this.checklist.items.length - 5;
  }

  editChecklist = () => {
    this.checklistManagementService.selectChecklist(this.checklist.id);
    this.goToEdit();
  }

  deleteChecklist = () => {
    this.checklistManagementService.deleteChecklist(this.checklist);
  }

  goToEdit = () => {
    this.router.navigate(['/edit']);
  }
}
