import { ChecklistItem } from '../../types/checklist-item';
import { Checklist } from '../../types/checklist';

export interface IChecklistItemFactory {
  doBuild(id: number): ChecklistItem;
  nextId(checklist: Checklist): number;
  getIds(checklist: Checklist, sort?: boolean): Array<number>;
}
