import { Checklist } from '../../types/checklist';

export interface IChecklistManagementService {
  getChecklists(): Array<Checklist>;
  searchChecklists(searchTerm: string): Array<Checklist>;
  getSelectedChecklist(): Checklist;
  getChecklist(id: number): Checklist;
  selectChecklist(id: number): void;
  newChecklist(): Checklist;
  updateChecklist(checklist: Checklist): void;
  deleteChecklist(checklist: Checklist): void;
}
