import { Checklist } from '../../types/checklist';

export interface IChecklistFactory {
  doBuild(id: number): Checklist;
}
