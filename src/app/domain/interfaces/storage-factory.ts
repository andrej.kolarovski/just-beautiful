import { Storage } from '../../types/storage';

export interface IStorageFactory {
  doBuild(): Storage;
  nextId(storage: Storage): number;
  getIds(storage: Storage, sort?: boolean): Array<number>;
}
