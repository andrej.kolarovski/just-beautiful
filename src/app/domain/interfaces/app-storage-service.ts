import { Storage } from '../../types/storage';
import { Checklist } from '../../types/checklist';

export interface IAppStorageService {
  initStorage(): void;
  newStorage(): Storage;
  getStorage(): Storage;
  getChecklists (): Array<Checklist>;
  searchChecklists(searchTerm: string): Array<Checklist>;
  getChecklistCount(): number;
  getSelectedChecklistId(): number;
  get(id: number): Checklist;
  select(id: number): void;
  update(checklist: Checklist): void;
  add(checklist: Checklist): void;
  delete(checklist: Checklist): void;
  nextId(): number;
  getLocalStorage(): Storage;
  updateLocalStorage(): void;
}
