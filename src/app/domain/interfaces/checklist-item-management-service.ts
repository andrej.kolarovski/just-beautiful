import { Checklist } from '../../types/checklist';
import { ChecklistItem } from '../../types/checklist-item';

export interface IChecklistItemManagementService {
  newItem(checklist: Checklist): ChecklistItem;
  addItem(checklist: Checklist, item: ChecklistItem): Checklist;
  removeItem(checklist: Checklist, item: ChecklistItem): Checklist;
}
