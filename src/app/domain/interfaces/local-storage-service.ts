import { Storage } from '../../types/storage';

export interface ILocalStorageService {
  clearStorage(): void;
  storageInitialized(): boolean;
  setStorage(storage: Storage): void;
  getStorage(): Storage;
}
