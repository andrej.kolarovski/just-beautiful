import { IChecklistItemFactory } from '../interfaces/checklist-item-factory';
import { ChecklistItem } from '../../types/checklist-item';
import { Checklist } from '../../types/checklist';

import { Injector, Injectable } from '@angular/core';

@Injectable()
export class ChecklistItemFactory implements IChecklistItemFactory {
  doBuild = (id: number): ChecklistItem => {
    const item = new ChecklistItem();
    item.id = id;
    item.completed = false;
    return item;
  }

  nextId = (checklist: Checklist): number => {
    const currentIds = this.getIds(checklist, true);
    for (let i = 0; i < currentIds.length; i++) {
      if (currentIds[i] > i) {
        return i;
      }
    }
    return currentIds.length;
  }

  getIds = (checklist: Checklist, sort?: boolean): Array<number> => {
    const items = checklist.items;
    const ret = items.map((item: ChecklistItem) => {
      return item.id;
    });
    if (sort) {
      ret.sort();
    }
    return ret;
  }
}
