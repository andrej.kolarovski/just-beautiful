import { IChecklistFactory } from '../interfaces/checklist-factory';
import { Checklist } from '../../types/checklist';

import { Injector, Injectable } from '@angular/core';

@Injectable()
export class ChecklistFactory implements IChecklistFactory {
  doBuild = (id: number): Checklist => {
    const checklist = new Checklist();
    checklist.id = id;
    checklist.items = [];
    checklist.tags = [];
    return checklist;
  }
}
