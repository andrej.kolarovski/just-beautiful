import { Storage } from '../../types/storage';
import { Checklist } from '../../types/checklist';
import { IStorageFactory } from '../interfaces/storage-factory';

export class StorageFactory implements IStorageFactory {
  doBuild = () => {
    const storage = new Storage();
    storage.items = [];
    storage.count = 0;
    storage.selectedChecklistId = -1;
    return storage;
  }

  nextId = (storage: Storage): number => {
    const currentIds = this.getIds(storage, true);
    for (let i = 0; i < currentIds.length; i++) {
      if (currentIds[i] > i) {
        return i;
      }
    }
    return currentIds.length;
  }

  getIds = (storage: Storage, sort?: boolean): Array<number> => {
    const checklists = storage.items;
    const ret = checklists.map((checklist: Checklist) => {
      return checklist.id;
    });
    if (sort) {
      ret.sort();
    }
    return ret;
  }
}
